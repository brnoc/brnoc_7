import React from 'react';

const team = require('../../resources/team.json')
export default function TeamPage(){
    return <div className="page" id="team-page">
        <main>
            <h1>Tým</h1>
            <h2>Aktivní členové</h2>
            <table>
                <thead>
                    <tr>
                        <td>Jméno</td>
                        <td>Role</td>
                    </tr>
                </thead>
                <tbody>        
                    {team.filter(i => i.active).map(person => <tr>
                            <td>{person.name}</td>
                            <td>{person.role}</td>
                    </tr>)}
                </tbody>
            </table>    
            <p></p>
            <h2>Rada starších</h2>
            <table>
                <thead>
                    <tr>
                        <td>Jméno</td>
                        <td>Role</td>
                    </tr>
                </thead>
                <tbody>        
                    {team.filter(i => !i.active).map(person => <tr>
                            <td>{person.name}</td>
                            <td>{person.role}</td>
                    </tr>)}
                </tbody>
            </table>    
        </main>
    </div>
}