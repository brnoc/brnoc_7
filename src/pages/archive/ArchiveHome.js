import React from 'react';
import './Archive.scss'
import { Link } from 'react-router-dom';

const archive = Object.values(require('../../resources/archive.json'))
export default function ArchiveHomePage(){
    return <div className="page" id="archive-home-page"> 
        <main>
            
            <p className="back">
                    <Link to="/">Zpět na hlavní starnu</Link>
            </p>
            <h1>Archiv BrNOCí</h1>
            {archive.map(event => <Link to={"/archive/"+event.id}><div className="event">
                <h2>{event.title}</h2>
                <p className="date">{event.date}</p>
            </div></Link>)}
        </main>
    </div>
}