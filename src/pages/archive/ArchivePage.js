import React from 'react';
import { BiCalendar, BiChalkboard } from 'react-icons/bi';
import { FaClock, FaUser, FaUsers } from 'react-icons/fa';
import { Link, Route, Switch } from 'react-router-dom';
import { Modal, Text } from '../../components/Components';

export default class ArchivePage extends React.Component{
    constructor(){
        super()
        this.state = {
            modal: -1
        }
    }

    render(){
        const archive = require('../../resources/archive.json')
        var brnoc = archive[this.props.context.match.params.id]
        if(!brnoc) return <div className="page">
            <main>
                <h1>Tuhle BrNOC jsme nenašli</h1>
                <Link to="/archive" className="link">Zpět do archivu</Link>
            </main>
        </div>
    
        brnoc = {
            talks: [], 
            ...brnoc
        }
    
        return <div className="page" id="archive-page">
            <main>
                <p className="back"><Link to="/archive">Zpět do archivu</Link></p>
                <h1>{brnoc.title}</h1>{/*
                <p className="header">
                    <Image imgSrc={brnoc.header} ratio={16/9}/>
                </p>*/}
                <div className="stats">
                    <div className="tile">
                        <BiCalendar/>
                        <div className="value">{brnoc.date}</div>
                    </div>
                    <div className="tile">
                        <FaUsers/>
                        <div className="value">{brnoc.people} účastníků</div>
                    </div>
                    <div className="tile">
                        <BiChalkboard/>
                        <div className="value">{Math.round(brnoc.talks.reduce((sum, talk) => sum+parseInt(talk.length), 0)/6)/10} h přednášek</div>
                    </div>
                </div>
                <h2>Přednášky</h2>
                <p>Klikni na přednášku pro zobrazení detailů.</p>
                <table>
                    <thead>
                        <tr>
                            <td>Název</td>
                            <td>Přednášející</td>
                            <td>Popis</td>
                            <td>Délka</td>
                            <td>Hodnocení</td>
                        </tr>
                    </thead>
                    <tbody>        
                        {brnoc.talks.map((talk, key) => <tr 
                            onClick={() => this.props.context.history.push("/archive/"+this.props.context.match.params.id+"/"+key)}
                        >
                            <td>{talk.title}</td>
                            <td>{talk.speaker}</td>
                            <td>{talk.short}</td>
                            <td>{talk.length} min</td>
                            <td>{talk.rating}</td>
                        </tr>)}
                    </tbody>
                </table>    
            </main>
            <Switch>
                <Route path="/archive/:id/:tid" render={ctx => <Modal ctx={ctx}
                        close = {() => ctx.history.push("/archive/"+ctx.match.params.id)}
                    >
                        {detail(brnoc.talks[ctx.match.params.tid])}
                    </Modal>}
                />
            </Switch>
        </div>
    }
}

function detail(talk){
    return <div className="talk-detail">
        <h3>{talk.title}</h3>
        <p className="icon-row">
            <FaUser/>
            <div>{talk.speaker}</div>
            <FaClock/>
            <div>{talk.length} min</div>
        </p>
        <p>
            {talk.short}
        </p>
        <p>
            <Text text={talk.long}/>
        </p>
    </div>
}