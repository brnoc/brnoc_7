import React from 'react';
import { Switch, Route } from 'react-router';
import ArchiveHome from './ArchiveHome';
import ArchivePage from './ArchivePage'

export default function ArchiveIndex(){
    return <Switch>
        <Route path="/archive/:id" render={ctx => <ArchivePage context={ctx}/>}/>
        <Route path="/archive"><ArchiveHome/></Route>
    </Switch>
}