import React from 'react';
import { FaFacebookF, FaInstagram, FaRegEnvelope } from 'react-icons/fa';
import { FiChevronDown } from 'react-icons/fi';
import { Link } from 'react-router-dom';
import Banner from '../components/banner/Banner';
import { Button, Carousel, Form, Image, TeamCard } from '../components/Components';
import './LandingPage.scss';

const team = require('../resources/team.json')
const partners = require('../resources/partners.json')
const nights = require('../resources/nights.json')


export default class LandingPage extends React.Component {
	constructor() {
		super()
		this.state = {
			status: "none",
			working: false,
		}
	}

	render() {
		return <div className="page" id="landing-page">
			<section id="header-section" className="full-height">
				<main>
					<h1>BrNOC</h1>
					<h2>Brněnská přednášková NOC</h2>
					<div className="info-text">
						{/*<p>
						BrNOC je Brněnská přednášková noc, kde studenti přednášejí studentům o svých odborných pracích, zájmech anebo prostě o tématech, která jim připadají důležitá. Jedná se o největší akci svého druhu v ČR!
						</p>*/}
						<p>
							Že už dlouho nebyla žádná BrNOC? Máme pro tebe řešení! Zaregistruj se na další akci na stránce <a href="https://rnoc.cz" className="link">R&ndash;NOC</a>.
						</p>
					</div>
					<h2>18.&mdash;19.06.2021</h2>
					<div className="register">
						<a href="https://rnoc.cz/register?night=2" ><Button>Registruj se</Button></a>
					</div>
					<div className="socials">
						<a href="https://facebook.com/brnoc.cz"><FaFacebookF /></a>
						<a href="https://instagram.com/brnoc5"><FaInstagram /></a>
						<a href="mailto://pus@brnoc.cz"><FaRegEnvelope /></a>
					</div>
				</main>
				<div className="scroll-down">
					<a href="#info-section"><div>Více informací</div>
						<FiChevronDown /></a>
				</div>
			</section>
			<section id="info-section">
				<main>
					<h2>R&ndash;NOC</h2>
					<p>
						Na letošní ročník se přednáškové NOCi z <a href="https://prenoc.cz" className="link">Prahy</a>, <Link to="/" className="link">Brna</Link>, <a href="https://plnoc.cz" className="link">Plzně</a> a <a href="https://hranol.gybon.cz" className="link">Hradce Králové</a> domluvily a uspořádají tak jednu akci v jeden den a se stejnými přednáškami, které se budou vysílat z jednoho města do druhého. Bude tak vyhověno epidemiologické situaci a zároveň nepřijdeš o pestrobarevnou škálu chutných přednášek. Registruj se i ty na <a href="https://rnoc.cz" className="link">této adrese</a>!
					</p>
				</main>
				<main>
					<h2>O BrNOCi</h2>
					<p>BrNOC je Brněnská přednášková noc, kde studenti přednášejí studentům. Jsi student základní, střední, či vysoké školy? Chceš si zkusit odpřednášet svou SOČku, či jinou odbornou přáci na nečisto? Chceš někomu povědět o svých koníčcích, nebo zvýšit povědomí o nějakém problému? Nebo si chceš poslechnout zajímavé prezentace od svých vrstevníků? Přihlaš se na další BrNOC!</p>
					<p>BrNOC se koná na gymnáziu na třídě Kapitána Jaroše v Brně</p>
					<div className="map">
						<div className="iframe">
							<iframe title="Kde najdu BrNOC?" src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d2606.92113754471!2d16.60900911591167!3d49.202055584400675!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x4712945c6c7feeb5%3A0x6bfcee2fbcd3d158!2zdMWZw61kYSBLYXBpdMOhbmEgSmFyb8WhZSBncmFtbWFyIHNjaG9vbA!5e0!3m2!1sen!2scz!4v1598121056549!5m2!1sen!2scz" frameborder="0" allowfullscreen="" aria-hidden="false" tabindex="0"></iframe>
						</div>
					</div>
					<p>Na archiv předchozích ročníků se můžeš podívat <Link to="/archive" className="link">zde</Link>.</p>
				</main>
			</section>
			<section id="team-section">
				<main>
					<h2>Náš tým</h2>
					<p>Brněnskou přednáškovou noc organizuje tým studentů z Gymnázia na třídě Kapitána Jaroše &mdash; Pravoúhlý sněm. Podívej se, kdo je jeho členem:</p>
					<div className="grid">
						{team.filter(i => i.active).map(i => <TeamCard {...i} />)}
					</div>
					<p>Seznam všech organizátorů včetně bývalých najdeš <Link to="/team" className="link">zde</Link>.</p>
				</main>
			</section>
			<section id="nights-section">
				<main>
					<h2>Další přednnáškové NOCi</h2>
					<p>BrNOC není zdaleka jedinou přednáškovou nocí v Česku! Pokud se ti tedy u nás líbilo a už se nemůžeš dočkat dalšího ročníku, určitě se podívej na tyto akce:</p>
					<div className="">
						<Carousel>
							{nights.map(i => <div className="night-card">
								<a href={i.url}><div className="picture">
									<Image ratio={1} variant="contain" imgSrc={i.imgSrc} />
								</div>
									<div className="name">
										{i.title}
									</div></a>
							</div>)}
						</Carousel>
					</div>
				</main>
			</section>
			<section id="partners-section">
				<main>
					<h2>Partneři</h2>
					<p>Bez těchto lidí, by naše akce nebyla možná:</p>
					<div className="grid">
						{partners.map(i => <div className="cell">
							<a href={i.url}><img alt={i.alt} src={i.imgSrc} /></a>
						</div>)}
					</div>
				</main>
			</section>
			<section id="contact-section">
				<main>
					<h2>Kontakt</h2>
					<p>Chceš nám něco říct, na něco se zeptat, pochválit web, či nám napsat cokoli jiného? Můžeš nás buď kontaktovat přes email <a href="mailto://pus@brnoc.cz">pus@brnoc.cz</a>, nebo využít tento formulář.</p>
					{this.state.working && <Banner type="loading" message="Odesílá se ..." />}
					{this.state.status === "ERROR" && <Banner type="error" message="Tvou zprávu se bohužel nepodařilo odeslat." />}
					{this.state.status === "SUCCESS" && <Banner type="success" message="Tvůj feedback byl úspěšně odeslán." />}
					<Form
						inputs={[
							{ type: "text", name: "name", placeholder: "Lorem Ipsum", label: "Jméno", required: true },
							{ type: "email", name: "email", placeholder: "dolor@sitamet.org", label: "E-mail", required: true, email: true },
							{ type: "textarea", name: "text", placeholder: "Co nám chceš říct?", label: "Text", required: true },
						]}
						button="Odeslat"
						submit={(data) => this.submitFeedback(data)}
					/>
				</main>
			</section>
		</div>
	}

	submitFeedback(data) {
		if (!(data.name && data.email && data.text && !this.state.working)) return;
		this.setState({ working: true, status: "NONE" })
		const xhr = new XMLHttpRequest();
		const form = new FormData();
		form.append("name", data.name)
		form.append("email", data.email)
		form.append("message", data.text)
		xhr.open("POST", "https://formspree.io/mdowvjjn")
		xhr.onreadystatechange = () => {
			if (xhr.readyState !== XMLHttpRequest.DONE) return;
			if (xhr.status === 0) {
				this.setState({ status: "SUCCESS", working: false })
			}
			else {
				this.setState({ status: "ERROR", working: false })
			}
		}
		xhr.send(form);
	}
}
