import Button from './button/Button';
import Image from './image/Image';
import TeamCard from './teamCard/TeamCard';
import Carousel from './carousel/Carousel';
import Form from './form/Form';
import Modal from './modal/Modal';
import Text from './text/Text'

export {Button, Image, TeamCard, Carousel, Form, Modal, Text}