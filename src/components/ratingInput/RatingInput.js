import React from 'react';
import './RatingInput.scss';
import { FaStar } from 'react-icons/fa';


/**
 * @param {element} icon
 * @param {int} range
 * @param {function} onChange 
 * @param {string} color 
 */
export default class RatingInput extends React.Component{
    constructor(props){
        super(props)
        this.state = {
            value:0
        }
    }

    componentDidUpdate(prevProps, prevState){
        if(prevState !== this.state){
            if(this.props.onChange)this.props.onChange(this.state.value)
        }
        if(prevProps.value !== this.props.value){
            if(typeof this.props.value !== "undefined")
                this.setState({value: this.props.value})
        }
    }

    render(){
        const props = {icon: <FaStar/>, range:5, onChange: () => {}, color:"", ...this.props,}

        var icons = [];

        for(var i = 1; i <= props.range; i++){
            const j = i;
            icons.push(<div 
                className={"icon "+(this.state.value>=j?"active":"")}
                style={{color: (this.state.value>=j?props.color:"")}}
                onClick={() => {
                    this.setState({value: j})
                }}
            >
                {props.icon}
            </div>)
        }

        return <div className={"ratingInput "+(this.props.required?"required ":" ")+(this.props.invalid?"invalid ":" ")}>
            <div className="label">{props.label}</div>
            <div className="icons">
                {icons}
            </div>
        </div>
    }
}