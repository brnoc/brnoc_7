import React from 'react';
import Linkify from 'react-linkify';
import './Text.scss'

export default function Text(props){
    var text=props.text?props.text:"";
    var rows = text.split("\n")
    var elements = rows.map(row => row[0]==="-"?
        <ul><li>{row.substr(1)}</li></ul>:
        <>{row}<br/></>
    );



    return <div className="linkified"><Linkify>
        {elements}
    </Linkify></div>
}