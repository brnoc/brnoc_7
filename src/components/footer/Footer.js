import React from 'react';
import './Footer.scss';

export default function Footer(){
    return <div id="footer">
        <main>
            <div className="copyright">
                &copy; Pravoúhlý spolek &ndash; všechna práva vyhrazena
            </div>
        </main>
        
    </div>
}