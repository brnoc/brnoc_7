import React from 'react';
import './Select.scss';
import {FaChevronDown} from 'react-icons/fa'

/**
 * @param {array} options [{value:"", label:""}]
 * @param {string} label
 * @param {string} placeholder
 * @param {function} onChange
 */
export default class Select extends React.Component{
    constructor(props){
        super(props)
        this.state = {
            selected: "",
            open: false,
        }
    }

    componentDidUpdate(prevProps, prevState){
        if(prevState.selected !== this.state.selected){
            if(this.props.onChange){
                this.props.onChange(this.state.selected)
            }
        }
        if(prevProps.value !== this.props.value){
            this.setState({selected: this.props.value})
        }
    }

    render(){
        const options = this.props.options;
        var text = <>&nbsp;</>;
        if(this.state.selected.length)text = options.filter(i => i.value===this.state.selected);
        if(text.length) text = text[0].label;
        else text = this.state.selected;
        return <div className={"selectWrapper "+(this.props.invalid?"invalid":"")}>
            
            <div className={"selected "+(this.state.open?"open":"")} onClick={() => this.setState({open: !this.state.open})}>
                <div className="text">{text}&nbsp;</div>
                <FaChevronDown/>
                <div className={"label "+(this.state.open||this.state.selected.length?"small":"")}>{this.props.label}</div>
                <div className={"placeholder "+(this.state.open&&!this.state.selected.length?"show":"")}>{this.props.placeholder}</div>
            </div>
            <div className={"options "+(this.state.open?"open":"")}>
                {options.map(i => <div className="option" onClick={ () =>{
                    this.setState({
                        open: false, selected: i.value
                    })
                }}>{i.label}</div>)}
            </div>
        </div>
    }
}