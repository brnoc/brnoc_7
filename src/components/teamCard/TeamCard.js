import React from 'react';
import { FaEnvelope, FaFacebookMessenger, FaYoutube } from 'react-icons/fa';
import {Image} from '../Components';
import './TeamCard.scss';

const SocialIcons = {
    "email":<FaEnvelope/>,
    "messenger": <FaFacebookMessenger/>,
    "youtube": <FaYoutube/>
}

export default function TeamCard(person){
    return <div className="team-card">
        <div className="picture">
            <Image ratio={1} imgSrc={person.imgSrc}/>
            <div className="info">
                <div className="name">{person.name}</div>
                <div className="role">{person.role}</div>
                <div className="contacts">
                    {Object.entries(person.contacts).map(i => SocialIcons[i[0]]?<a href={i[1]}>{SocialIcons[i[0]]}</a>:"")}
                </div>
            </div>
        </div>
    </div>
}
