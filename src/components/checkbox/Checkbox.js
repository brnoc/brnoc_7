import React from 'react';
import './Checkbox.scss';

export default function Checkbox(props){
    return <div className={"checkboxWrapper "+(props.invalid?"invalid":"")}>
        <input type="checkbox" {...props}/>
        <div className="box"></div>
        <div className="label">{props.label}</div>
    </div>
}