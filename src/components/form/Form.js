import React from 'react'
import Input from '../input/Input'
import Textarea from '../textarea/Textarea'
import Button from '../button/Button'
import Banner from '../banner/Banner'

import './Form.scss';
import Checkbox from '../checkbox/Checkbox'
import ImageInput from '../imageInput/ImageInput'
import ListInput from '../listInput/ListInput'
import Select from '../select/Select'
import RatingInput from '../ratingInput/RatingInput'

//TODO> min delka hesla 6 znaku

const validators = {
    minLength: (val, min) => {
        if(!val) val = ""
        return Boolean(val.length >= min)
    },
    maxLength: (val, max) => {
        if(!val) val = ""
        return Boolean(val.length <= max)
    },
    min: (val, min) => {
        if(val < min)return false;
        return true
    },
    max: (val, max) => {
        if(val > max)return false;
        return true;
    },
    email: (val, x) => {
        var re = /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/
        return re.test(val)
    },
    required: (val, x) => {
        if(typeof val === "number") return true;
        if(!val) return false;
        return true;
    }
}

function koncovka(num){
    num = Math.abs(num)
    if(num === 0) return "ů"
    if(num === 1) return ""
    if(num < 5) return "y"
    return "ů"
}

const errorMessages = {
    minLength: (field, min) => <>Pole <b>{field}</b> má minimální délku <b>{min}</b> znak{koncovka(min)}</>,
    maxLength: (field, max) => <>Pole <b>{field}</b> má maximální délku <b>{max}</b> znak{koncovka(max)}</>,
    email: (field, x) => <>Pole <b>{field}</b> musí obsahovat valdiní e-mailovou adresu.</>,
    required: (field, x) => <>Pole <b>{field}</b> je povinné.</>,
    min: (field, min) => <>Minimální hodnota pole <b>{field}</b> je <b>{min}</b>.</>,
    max: (field, max) => <>Maximální hodnota pole <b>{field}</b> je <b>{max}</b>.</>,
}


/**
 * This is a form component
 * 
 * @prop {array} inputs array of inputs {name, type, label, placeholder, ...validators}
 * if input has type section, it is transformed into <h2/> {type: 'section', text: ''}
 * @prop {object} values
 * @prop {function} dataUpdated
 * @prop {function} submit
 * @prop {string} button
 * @prop {bool} hideErrors
 * @prop {string} name id of the form element, needed to scroll up after submit
 * 
 * V: 1.5.1
 * Last updated: 20. 8. 2020
 * Dependencies: Input, Textarea, Button, Banner, Checkbox, ListInput, ImageInput, Select, RatingInput
 */

export default class Form extends React.Component{
    constructor(props){
        super(props)
        this.state = {
            errors: [],
            values: {

            }
        }
    }

    componentDidUpdate(prevProps, prevState){
        if(this.state.values !== prevState.values || this.state.errors !== prevState.errors){
            if(typeof this.props.dataUpdated === "function")
            this.props.dataUpdated({
                errors: this.state.errors,
                values: this.state.values,
                valid: !this.state.errors.length
            })
        }
        if(this.state.values !== prevState.values && this.state.errors.length){
            this.validate()
        }
        if(this.props.values !== prevProps.values){
            this.setState({values: this.props.values})
        }
    }

    async validate(){
        if(typeof this.props.inputs !== "object") return;
        await this.setState({errors: []})
        for(var inputKey in this.props.inputs){
            const input = this.props.inputs[inputKey]
            for(var criterion in validators){
                if(input[criterion]){
                    if(! validators[criterion](this.state.values[input.name], input[criterion])){
                        var errors = this.state.errors;
                        errors.push({name:input.name, message: errorMessages[criterion](input.label, input[criterion])})
                        this.setState({errors: errors})
                    }
                }
            }
        }
    }

    inputs(){
        if(typeof this.props.inputs !== "object") return [];
        var inputs = [];
        for(var key in this.props.inputs){
            const input = this.props.inputs[key]
            if(["text","password","email", "date", "time", "datetime", "number"].includes(input.type)){
                inputs.push(<Input
                    {...input}
                    invalid={Boolean(this.state.errors.filter(error => error.name === input.name).length)}
                    onChange={e => this.setState({
                        values: {...this.state.values, [input.name]: e.target.value}
                    })}
                    onKeyUp = {e => {if(e.key === "Enter") this.submit()}}
                    value = {this.state.values[input.name]}
                />)
            }
            if(["textarea"].includes(input.type)){
                inputs.push(<Textarea
                    {...input}
                    invalid={Boolean(this.state.errors.filter(error => error.name === input.name).length)}
                    onChange={e => this.setState({
                        values: {...this.state.values, [input.name]: e.target.value}
                    })}
                    value = {this.state.values[input.name]}
                />)
            }   
            if(["checkbox"].includes(input.type)){
                inputs.push(<Checkbox
                    {...input}
                    invalid={Boolean(this.state.errors.filter(error => error.name === input.name).length)}
                    onChange={e => this.setState({
                        values: {...this.state.values, [input.name]: e.target.checked}
                    })}
                    onKeyUp = {e => {if(e.key === "Enter") this.submit()}}
                    value = {this.state.values[input.name]}
                />)
            }      
            if(["image"].includes(input.type)){
                inputs.push(<ImageInput
                    {...input}
                    invalid={Boolean(this.state.errors.filter(error => error.name === input.name).length)}
                    onChange={e => this.setState({
                        values: {...this.state.values, [input.name]: e.target.files[0]}
                    })}
                    onKeyUp = {e => {if(e.key === "Enter") this.submit()}}
                />)
            }    
            if(["list"].includes(input.type)){
                inputs.push(<ListInput
                    {...input}
                    invalid={Boolean(this.state.errors.filter(error => error.name === input.name).length)}
                    onChange={val => this.setState({
                        values: {...this.state.values, [input.name]: val}
                    })}
                    onKeyUp = {e => {if(e.key === "Enter") this.submit()}}
                    values = {this.state.values[input.name]}
                />)
            }    
            if(["select"].includes(input.type)){
                inputs.push(<Select
                    {...input}
                    invalid={Boolean(this.state.errors.filter(error => error.name === input.name).length)}
                    onChange={val => this.setState({
                        values: {...this.state.values, [input.name]: val}
                    })}
                    onKeyUp = {e => {if(e.key === "Enter") this.submit()}}
                    value = {this.state.values[input.name]}
                />)
            }   
            if(["rating"].includes(input.type)){
                inputs.push(<RatingInput
                    {...input}
                    invalid={Boolean(this.state.errors.filter(error => error.name === input.name).length)}
                    onChange={val => this.setState({
                        values: {...this.state.values, [input.name]: val}
                    })}
                    value = {this.state.values[input.name]}
                />)
            }
            if(["section"].includes(input.type)){
                inputs.push(<h2>
                    {input.text}
                </h2>)
            }         
            if(["p"].includes(input.type)){
                inputs.push(<p>
                    {input.text}
                </p>)
            }     
        }
        return inputs;
    }

    errors(){
        return this.state.errors.map(error => <Banner type="error" message={error.message}/>)
    }

    
    submit(){
        const callback = this.props.submit ? this.props.submit: () => console.warn("Form: No submit function provided")
        this.validate().then(() => {
            if(!this.state.errors.length) 
                callback(this.state.values)
            if(this.props.name){
                var el = document.getElementById(this.props.name);
                if(el){
                    window.scrollTo({top:el.offsetTop - 250})
                }
                
            }
        })
    }

    render(){
        return <div className={"formWrapper " + (this.props.small?"small":"")} id={this.props.name?this.props.name:""}>
            {!this.props.hideErrors&&<div className="errors">
                {this.errors()}
            </div>}
            <form>
                {this.inputs()}
            </form>
            <div className="buttonWrapper">
                <Button variant={this.props.redButton?"danger":""} onClick={() => this.submit()}>{this.props.button}</Button>
            </div>
        </div>
    }
}