import React, {Component} from 'react';
import ReactDOM from 'react-dom';
import './Carousel.scss';

import { FaChevronLeft, FaChevronRight } from 'react-icons/fa'


class Carousel extends Component {
    constructor(props){
        super(props);

        this.rootRef = React.createRef();
        this.itemsRef = React.createRef();
        this.varComputeRef = React.createRef();
        this.controlsRef = React.createRef();

        this.aviable_position_count = null;

        this.position = 0;

        this._touch_tmp_X = 0;
        this._fisrtRender = true;
        this.__width = 0;

        this.touchStart = this.touchStart.bind(this);
        this.touchEnd = this.touchEnd.bind(this);

        this.scrollTo = this.scrollTo.bind(this);
        this.scrollBy = this.scrollBy.bind(this);

        this.selectPosition = this.selectPosition.bind(this)
        this.controlsDrag = this.controlsDrag.bind(this)
        this._controlsDrag = false;

        window.addEventListener('mouseup',()=>{
            this._controlsDrag = false;
        })
        window.addEventListener('touchend',e=>{
            this._controlsDrag = false;
        })
    }   


    render(){
        /* */
        const ipw = this.props.itemsPerView?this.props.itemsPerView:3
        return (
            <div className='carousel' ref={ this.rootRef } style={{'--parent-width': this.__width + 'px', '--items-per-view':ipw}} >
                <div className='items' ref={ this.itemsRef } onTouchStart={this.touchStart} onTouchEnd={this.touchEnd}>
                    { this.props.children}
                </div>
                
                <div className='controls' ref={ this.controlsRef }>
                    <FaChevronLeft onClick={()=>this.scrollBy(-1)} ontTouchStart={e=>(e.preventDefault(),this.scrollBy(-1))}/>
                        { !this._fisrtRender ? this.getAvailablePositions() : []}
                    <FaChevronRight onClick={()=>this.scrollBy(1)} ontTouchStart={e=>(e.preventDefault(),this.scrollBy(1))}/>
                </div>
                <div className="_var-compute" ref={ this.varComputeRef }></div>
            </div>
 
        )
    }



    componentDidMount() {
        if(this._fisrtRender) {
            this._fisrtRender = false;
            this.forceUpdate();
        }

        this.itemsRef.current.scrollTo(0,0);
        window.addEventListener('resize', ()=>this.forceUpdate());
    }
    
    componentWillUnmount(){
        window.removeEventListener('resize', ()=>this.forceUpdate());
    }

    componentWillUpdate(){
        let parentStyle = getComputedStyle(this.rootRef.current.parentElement);
        this.__width = this.rootRef.current.parentElement.getBoundingClientRect().width - parseFloat(parentStyle.paddingLeft) - parseFloat(parentStyle.paddingRight);       ;
    }

    componentDidUpdate() { 
        this.scrollBy(0);
    }

    getAvailablePositions(){
        let cards = !this.props.children[Symbol.iterator] ? [this.props.children] : this.props.children;
        let aviable_positions = [];
        this.aviable_position_count = cards.length - (this.computeCssVariable('items-per-view')-1);
        this.aviable_position_count = this.aviable_position_count <= 0 ? 1 : this.aviable_position_count;

        for(let i=0; i < this.aviable_position_count; i++) aviable_positions.push(
             <div 
             className="dot" 
             key={i} 
             onMouseDown={()=>this.selectPosition(i)}
             onTouchStart={e=>{e.preventDefault();this.selectPosition(i)}}
             onMouseOver={()=>this._controlsDrag ? this.scrollTo(i): null}
             onTouchMoveCapture={e=>(e.preventDefault(),this._controlsDrag && this.controlsDrag(e.changedTouches[0].pageX) ? this.scrollTo(i): null)}
             ></div> )
        return aviable_positions;
    }
    
    scrollTo(index){
        let dots = this.controlsRef.current.getElementsByClassName('dot');
        if(dots[this.position]) dots[this.position].removeAttribute('active')

        this.position = index % this.aviable_position_count;
        this.position = this.position < 0 ? this.position + this.aviable_position_count : this.position;

        this.itemsRef.current.scrollTo(this.position * (this.computeCssVariable('item-size') + this.computeCssVariable('item-gap')),0)
        
        dots[this.position].setAttribute('active','true');
    }

    scrollBy(direction){
        this.scrollTo(this.position+direction)
    }

    selectPosition(i){
        if(i != this.position) this.scrollTo(i);
        this._controlsDrag = true;
    }

    controlsDrag(client_x){
        if(!this._controlsDrag) return;
        let controlsRect = this.controlsRef.current.getBoundingClientRect();
        let index = Math.round((client_x - controlsRect.left)/controlsRect.width* this.aviable_position_count);
        if(index != this.position) this.scrollTo(index)         
    }

    computeCssVariable(v){
        this.varComputeRef.current.setAttribute('variable',v)
        return this.varComputeRef.current.getBoundingClientRect().width;
    }

    
    touchStart(e){
        e.preventDefault();
        this._touch_tmp_X = e.touches[0].clientX;
    }


    touchEnd(e){
        if(Math.abs(this._touch_tmp_X - e.changedTouches[0].clientX) > window.innerWidth/10)
            this.scrollBy(this._touch_tmp_X > e.changedTouches[0].clientX   ? 1 : -1);
    }


}

export default Carousel;