import React from 'react';
import Image from '../image/Image';
import './ImageInput.scss';

/**
 * @param {number} ratio
 * @param {string} text
 * @param {string} label
 * @param {function} onChange
 */
export default class ImageInput extends React.Component{
    constructor(props){
        super(props)
        this.state = {
            imgSrc: "",
        }
    }

    componentDidUpdate(prevProps){
        if(this.props.imgSrc !== prevProps.imgSrc){
            if(this.props.imgSrc)this.setState({imgSrc:this.props.imgSrc});
            else if(this.props.file)this.setState({imgSrc: URL.createObjectURL(this.props.file)})
        }
    }

    getImage(event){
        if(!event.target.files.length)return;
        this.setState({
            imgSrc: URL.createObjectURL(event.target.files[0])
        })
    }

    render(){
        const props = {
            ratio: 1,
            text: "",
            label: "",
            onChange: () => {},
            ...this.props
        }

        return <div className={"imageInputWrapper "+(this.props.invalid?"invalid":"")}>
            <div className="picture">
                <Image ratio={props.ratio} imgSrc={this.state.imgSrc}/>
                <input {...this.props} type="file" onChange={(e) => {this.getImage(e); props.onChange(e)}}/>
                <div className="overlay">{props.text}</div>
                <div className={"label "+(this.state.imgSrc?"small":"")}>{props.label}</div>
            </div>
            
        </div>
    }
}