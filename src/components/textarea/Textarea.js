import React from 'react';
import './Textarea.scss';

/**
 * Textarea component
 * @param {string} label
 * all props are passed on the textarea element
 */
export default function Textarea(props){
    return <div className={"textareaWrapper "+(props.invalid?"invalid":"")}>
        <textarea {...props}></textarea>
        <div className="label">{props.label}</div>
    </div>
    
}