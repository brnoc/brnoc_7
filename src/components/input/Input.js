import React from 'react';
import './Input.scss';

/**
 * Input component
 * @param {string} label Input label
 * all props are passed to the input element
 */
export default function Input(props){
    props = {
        placeholder: " ",
        ...props    
    }
    return <div className={"inputWrapper "+(props.invalid?"invalid":"")}>
        <input {...props}/>
        <div className="label">{props.label}</div>
    </div>
}