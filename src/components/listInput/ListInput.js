import React from 'react';
import './ListInput.scss';
import Input from '../input/Input';
import { FaChevronUp, FaChevronDown, FaTrash } from 'react-icons/fa';

export default class ListInput extends React.Component{
    constructor(props){
        super(props)
        this.state = {
            items: [],
            input: ""
        }
    }

    componentDidUpdate(prevProps, prevState){
        if(prevState.items !== this.state.items){
            if(this.props.onChange){
                this.props.onChange(this.state.items)
            }
        }

        if(prevProps.values !== this.props.values){
            this.setState({items: this.props.values})
        }
    }

    add(item){
        var items = [...this.state.items]
        items.push(item);
        this.setState({items: items})
    }

    remove(id){
        var items = [...this.state.items]
        items.splice(id, 1)
        this.setState({items: items})
    }

    move(id, up = true){
        var items = [...this.state.items]
        items[id] = items.splice(up?id-1:id+1, 1, items[id])[0];
        this.setState({items: items})
    }

    render(){
        return <div className="listInputWrapper">
                <div className="top"><Input placeholder={this.props.placeholder} label={this.props.label} onChange={e => this.setState({input: e.target.value})} value={this.state.input}/><div className="button" onClick={() => {this.add(this.state.input); this.setState({input:""})}}>Přidat</div></div>
                {this.state.items.map((item, id) => 
                    <div className="item">
                        <div>{item}</div>
                        <div className="controls">
                            <FaChevronUp className={(id===0)?"disabled":""} onClick={() => {if(id>0)this.move(id, true)}}/>
                            <FaChevronDown className={(id===(this.state.items.length-1))?"disabled":""} onClick={() => {if(id<(this.state.items.length-1))this.move(id, false)}}/>
                            <FaTrash onClick={() => this.remove(id)}/>
                        </div>
                    </div>)}
        </div>
    }
}