import React from 'react';
import { FaTimes } from 'react-icons/fa';
import './Modal.scss';

export default function Modal({children, close, hideCloseButton=false}){
    return <div className="modalWrapper">
        <div className="overlay" onClick={() => close()}></div>
        <div className="modal" >
            {!hideCloseButton&&<div className="close" onClick={() => close()}><FaTimes/></div>}
            {children}
        </div>
    </div>
}