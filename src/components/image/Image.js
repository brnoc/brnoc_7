import React from 'react';


/**
 * Image component
 * @param {string} props.imgSrc Image 
 * @param {number} props.ratio Image div width:height ratio -- ex. {16/9}
 * @param {string} props.variant background size -- options ["contain", "cover"] default: "cover"
 * all props are passed to child
 */
export default function Image(props){
    const {imgSrc="", ratio=1, variant="cover", ...p} = props
    return <div className="image" {...p}>
        <div className="imageInner" style={{
            backgroundImage: "url("+imgSrc+")",
            paddingTop: (100/ratio)+"%",
            backgroundSize: variant==="contain"?"contain":"cover",
            backgroundPosition: "center",
            backgroundRepeat:"no-repeat",
            width: "100%",
        } }>

        </div>
    </div>
}

/**
 * Image component with asynchronous url
 * 
 * @param {function} asyncSrc
 * all props are passed to child
 */
export class AsyncImage extends React.Component{
    constructor(props){
        super(props)
        this.state = {
            src: "",
            bg:"#FFF"
        }
        this.load()
    }

    async load(){ 
        try{
            this.setState({src: await this.props.asyncSrc()})
        }catch(e){
            console.log(e);
        }
        
    }

    componentDidUpdate(prevProps){
        if(this.props.asyncSrc !== prevProps.asyncSrc)this.load()
    }

    render(){
        return <Image imgSrc={this.state.src} {...this.props}/>
    }
}