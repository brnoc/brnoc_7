import React from 'react';
import { BrowserRouter, Route, Switch } from 'react-router-dom';
import './App.scss';
import Footer from './components/footer/Footer';
import ArchiveIndex from './pages/archive/ArchiveIndex';
import LandingPage from './pages/LandingPage';
import SchedulePage from './pages/schedule/SchedulePage';
import TalksPage from './pages/talks/TalksPage';
import TeamPage from './pages/team/TeamPage';

function App() {
  return (
    <div id="app">
      <BrowserRouter>
          <Switch>
            <Route path="/archive"><ArchiveIndex/></Route>
            <Route path="/schedule"><SchedulePage/></Route>
            <Route path="/team"><TeamPage/></Route>
            <Route path="/talks"><TalksPage/></Route>
            <Route path="/"><LandingPage/></Route>
          </Switch>
          <Footer/>
      </BrowserRouter>
    </div>
  );
}

export default App;
